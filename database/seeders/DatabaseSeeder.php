<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       $courses = ['Antipasti', 'Primi', 'Secondi', 'Contorni', 'Dolci'];

       foreach ($courses as $course){

            DB::table('courses')->insert([
                'name'=> $course,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
            ]);
        }
    }
}