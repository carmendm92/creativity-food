<x-layout
    title="Creativity Food"
    description="Portale ricco di ricette golose"
>
    <div class="container">
        <div class="row justify-content-center align-items-center">

{{-- messaggi di errore --}}
        <div class="my-5">
            @if(session('message'))
            <div class="alert alert-info">
                {{ session('message') }}
            </div>
            @endif

            @if(session('submit'))
            <div class="alert alert-success">
                {{ session('submit') }}
            </div>
            @endif

            @if(session('error'))
            <div class="alert alert-warning">
                {{ session('error') }}
            </div>
            @endif

            
            @if(session('destroy'))
            <div class="alert alert-danger">
                {{ session('destroy') }}
            </div>
            @endif
        </div>

{{--Titolo pagina--}}
            <div class="col-12 text-center">
                <h1 class="fw-bolder m-5 fa-3x text-danger">{{__('Ultime ricette')}}</h1>
            </div>
{{-- Card --}}
            @foreach ($recipes as $recipe)
                <div class="col-12 col-md-5 border-end border-start border-danger mx-2 my-5" itemscope itemtype="https://schema.org/Recipe">
                    <div class="row align-items-center justify-content-center g-0">
                        <div class="container-title-card">
                            <div itemprop="recipeName">
                                <h2 class="card-title text-center fw-bolder">{{$recipe->title}}</h2>
                            </div>
                            <div class="text-center">
                                <a class="btn text-decoration-underline " href="{{route('recipe.course', ['course'=>$recipe->course->id])}}">
                                    {{$recipe->course->name}}
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="card-body container-text-card">
                                <span class="card-text" itemprop="abstract">
                                   {{Str::limit($recipe->description,100)}}
                                </span>
                            </div>
                        </div>
                        <div class="col-12 col-lg-4 text-center">
                            <img itemprop="image" class="img-fluid p-2 border border-danger" alt="dettaglio del piatto {{$recipe->title}}" src="{{$recipe->images->first()?$recipe->images->first()->getUrl(150,150) : ''}}">
                        </div>
                        <div class="my-2 p-3 data-card">
                            <meta class="card-text" itemprop="datePublished" content="05-11-2021">
                                    <small>{{__('Pubblicata: ')}}{{$recipe->created_at->format('d.m.Y')}}</small>
                        </div>
                        <div class=text-center>
                            <a href="{{Route('recipe.show', compact('recipe'))}}" class=" m-4 btn btn-click">{{__('Leggi ricetta')}}</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</x-layout>