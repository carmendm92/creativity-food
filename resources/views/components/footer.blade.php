<footer>
    <div class="container footer d-flex align-items-space-around justify-content-center mt-5">
        <div class="row">
            <div class="col-12 col-md-6 text-center d-flex justify-content-center align-items-center">
                <button class="button-04">

                    <span itemscope itemtype="https://schema.org/follows"class="social">
                        <a itemprop="follows" href="https://www.instagram.com/crea_ty.food/" class="button-04-box">
                            <i class="fab fa-instagram"></i>
                        </a>
                
                        <a itemprop="follows" href="https://www.pinterest.com/creatyfood" class="button-04-box">
                            <i class="fa-brands fa-pinterest"></i>
                        </a>
                    </span>
                    
                    <span>Follow me</span>
                </button>
            </div>
            <div class="col-12 col-md-6 border-start border-danger text-center d-flex justify-content-center align-items-center">
            <span class="text-center" itemprop="email">We would love to hear from you <br>
                <a class="btn text-dark fw-bolder" href="mailto:crea.ty.food@gmail.com">
                    crea.ty.food@gmail.com
                </a>
            </span>
            </div>

            <div class="col-12 mt-5 d-flex justify-content-center align-items-end border-top border-danger">
                <p class="text-center">
                    © 2021 Creativityfood</p>
            </div>
            <div class="col-12 mb-3 text-center">
                <a href="https://www.iubenda.com/privacy-policy/32381562" class="btn iubenda-noiframe iubenda iubenda-noiframe " title="Privacy Policy">Privacy Policy</a>
            </div>
        </div>
    </div>
</footer>