<nav class="navbar navbar-expand-lg sticky-top flex-wrap navbar-light justify-content-center border-bottom border-danger navbar_color m-0">
  <div class="container-fluid">
    <a class="fw-bolder navbar-brand" href="{{route('homepage')}}">Home</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <i class="m-0 navbar-brand fa-solid fa-bars"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ms-auto mb-2 mb-lg-0 border-none">
    @auth
      @if(Auth::user()->is_admin)
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              {{__('Benvenuto')}} {{Auth::user()->name}}!
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="nav-link" href="{{route('recipe.create')}}">{{__('Inserisci Ricetta')}}</a>
              <li><hr class="dropdown-divider"></li>
              <li>
                <a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('form-logout').submit();">Logout</a>
              </li>
              <form action="{{route('logout')}}" method="POST" id="form-logout">
              @csrf
              </form>            
            </ul>
          </li>
          @endif
      @endauth
          <li class="nav-item">
            <a class="nav-link" href="{{ route('contact') }}">About Me</a>
        </li>
      </ul>  
    </div>
  </div>

  <div class="row border-top border-danger pt-2 collapse navbar-collapse justify-content-center m-0 py-0" id="navbarSupportedContent">
    @foreach ($courses as $course)
      <div itemscope itemtype="https://schema.org/Recipe" class="col-md-2 border-course my-2 d-flex justify-content-center align-items-center">
        <a class="button-02 text-center nav-item" href="{{route('recipe.course',compact('course'))}}">
            <span class="button-2-icon"><i class="fa-solid fa-utensils"></i></span>
            <span itemprop="recipeCategory">{{$course->name}}</span>
        </a>
      <hr class="dropdown-divider text-danger">
      </div>
    @endforeach
  </div>
</nav>