<x-layout
    title="About me"
    description="Contatti"
>
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <h1 class="my-5 fw-bolder text-center text-danger fa-3x">
                {{__('Chi sono')}}
            </h1>
{{-- Place --}}
            <div class="col-12 col-md-6 my-4 itemscope" itemtype="https://schema.org/Person">
                <span itemprop="addressLocality">
                    <h2 class="text-center mb-3">{{__('Sicilia in provincia di Catania')}}</h2>
                </span>
                <div class="text-center google-maps border border-dark">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d173817.4722508886!2d15.093435907370408!3d37.61255825227098!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sit!2sit!4v1641489790731!5m2!1sit!2sit" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
            </div>
                
            <div class="col-12 col-md-6 my-5 text-center">
                <p class="p-4">
                    {{__('Amante della cucina e del buon cibo, mi diletto a sperimentare con ingredienti semplici, da web developer mi sono creata un sito dove
                    inserire tutte le ricette. Spero possano essere utili a qualcuno.')}}
                </p>
                <h3 class="text-center itemscope">{{__('Contatti personali')}}</h3>
                <button class="btn">
                    <span itemprop="follows" class="d-flex justify-content-space-around">
                        <a itemprop="url" href="https://www.linkedin.com/in/carmen-di-mauro/" class="button-04-box m-1">
                            <i class="fab fa-linkedin"></i>
                        </a>
                        <a itemprop="url" href="https://gitlab.com/carmendm92" class="button-04-box m-1">
                            <i class="fa-brands fa-gitlab"></i>
                        </a>

                        <a itemprop="url" href="https://www.instagram.com/c.a.r.m.e.n.9.2/" class="button-04-box m-1">
                            <i class="fa-brands fa-instagram"></i>
                        </a>
                    </span>
               
                </button>
            </div>
        </div>
    </div>
    <div class="container my-5">
        <div class="row justify-content-center">
            <h3 class="fw-bolder text-center my-5">{{__('Seguimi sui social')}}</h3>
            <div class="col-12 col-md-6">
                <a data-pin-do="embedBoard" data-pin-lang="it" data-pin-board-width="400" data-pin-scale-height="240" data-pin-scale-width="80" href="https://www.pinterest.it/creatyfood/ricette/"></a>
            </div>
            <div class="col-12 col-lg-4 p-2 border rounded-3">
                <iframe src="https://snapwidget.com/embed/981433" class="snapwidget-widget" allowtransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden;  width:100%; "></iframe>
                <a class="text-white btn follow_instagram fw-bolder" itemprop="url" href="https://www.instagram.com/crea_ty.food">
                    Segui su <i class="fab fa-instagram"></i> Instagram
                </a>
            </div>
        </div>
    </div>
</x-layout>