<?php

namespace App\Http\Controllers;


use App\Models\Course;
use App\Models\Recipe;
use App\Jobs\ResizeImage;
use App\Models\RecipeImage;
use Illuminate\Http\Request;
use App\Http\Requests\RecipeRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class RecipeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }
   
//  mostra il form per creare la risorsa
    public function create(Request $request)
    {
        $uniqueSecret = $request->old(
            'uniqueSecret',
            base_convert(sha1(uniqid(mt_rand())), 16, 36)
        );


        $courses = Course::all();


        return view('recipe.create', compact('uniqueSecret'));
    }

//    mostra la risorsa creata
    public function store(RecipeRequest $request)
    {   
        $courses = Course::all();
        $recipe = Auth::user()->recipes()->create([
             
            'title'=>$request->title,
            'ingredients'=>$request->ingredients,
            'preparation'=>$request->preparation,
            'course_id'=>$request->course_id,
            'description'=>$request->description,
        ]);
        $uniqueSecret = $request->input('uniqueSecret');

        $images = session()->get("images.{$uniqueSecret}",[]);
        $removedImages = session()->get("removedimages.{$uniqueSecret}",[]);
        
        $images= array_diff($images, $removedImages);

        foreach($images as $image){
            $i = new RecipeImage();
            
            $fileName = basename($image);
            $newFileName = "public/recipes/{$recipe->id}/{$fileName}";
            Storage::move($image, $newFileName);

            dispatch(new ResizeImage(
                $newFileName,
                500,
                480
            ));

            dispatch(new ResizeImage(
                $newFileName,
                150,
                150
            ));
            
            $i->file = $newFileName;
            $i->recipe_id = $recipe->id;

            $i->save();
        }

        File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));

        return redirect(route('homepage'))->with('message', 'La tua ricetta è stata inserita');

    }

//  Funzione per l'inserimento delle immagini
    public function uploadImage(Request $request)
    {
        $uniqueSecret = $request->input('uniqueSecret');
        $fileName = $request->file('file')->store("public/temp/{$uniqueSecret}");

        dispatch(new ResizeImage(
            $fileName,
            120,
            120
        ));


        session()->push("images.{$uniqueSecret}", $fileName);
        return response()->json(
            [
            'id' =>$fileName
            ]
        );
        
    }

//  funzione per eliminare le immagini
    public function removeImage(Request $request)
    {

        $uniqueSecret = $request->input('uniqueSecret');

        $fileName = $request->input('id');

        session()->push("removedimages.{$uniqueSecret}", $fileName);

        Storage::delete($fileName);
        
        return response()->json('ok');
    }

//  funzione per salvare le immagini
    public function getImages(Request $request)
    {
        $uniqueSecret = $request->input('uniqueSecret');
        $images = session()->get("images.{$uniqueSecret}", []);
        $removedImages = session()->get("removedimages.{$uniqueSecret}", []);

        $images = array_diff($images, $removedImages);

        $data =[];

        foreach ($images as $image){
            $data[] = [
                'id' =>$image,
                'src' =>RecipeImage::getUrlByFilePath($image,120,120),
            ];
        }
        return response()->json($data);
    }


//   mostra il form per modificare la risorsa
    public function edit(Recipe $recipe)
    {
       return view('recipe.edit', compact('recipe'));
    }

//    mostra la risorsa modificata
    public function update(RecipeRequest $request, Recipe $recipe)
    {   
        $recipe->update([
            'title'=>$request->title,
            'ingredients'=>$request->ingredients,
            'preparation'=>$request->preparation,
            'description'=>$request->description,

        ]);

        return redirect (route('recipe.show', compact('recipe')))->with('message', 'ricetta modificata');
    }

//   elimina la risorsa
    public function destroy(Recipe $recipe)
    {   
        foreach(
            $recipe->images()->get() as $image 
        ){
            $image->delete();
        }
       
        $recipe->delete();
        return redirect (route('homepage'))->with('destroy', 'ricetta eliminata');
    }
}
