<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class MakeUserAdmin extends Command
{
 
    protected $signature = 'food:makeUserAdmin';
    protected $description = 'Rendi l\'utente admin';

    
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $email = $this->ask('Inserisci l\'email dell\'utente che vuoi rendere admin');

        $user = User::where('email', $email)->first();

        if(!$user){
            $this->error("Utente non trovato");
            return;
        }
        $user->is_admin = true;
        $user->save();
        $this->info("l'utente {$user->name} è ora admin.");
    }
}
